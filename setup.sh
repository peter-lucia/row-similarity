
# one time only
virtualenv --python=python3.5 venv 
source venv/bin/activate
pip3 install jupyter

# neovim autocompletion
pip3 install neovim
pip3 install jedi
pip3 install python-language-server
pip3 install --upgrade pip

