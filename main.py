import numpy as np
import pandas as pd
import h5py

df = pd.read_csv("./DEIDd_Data.csv")
df = df.set_index("ID")
rows = len(df)

combinations = set()
for i in range(1, rows):
    for j in range(i, rows):
        if i == j:
            continue
        combinations.add(frozenset({i,j}))

rows, columns = len(df), len(df)
f = h5py.File("output2.hdf5", "w")

dset = f.create_dataset("perc_sim", (rows, columns))

while combinations:
    print("combinations left: {0}".format(len(combinations)))
    ids = combinations.pop()
    id1, id2 = ids
    row1 = df.loc[id1].tolist()
    row2 = df.loc[id2].tolist()
    assert len(row1) == len(row2)
    diff = [(row1[i] - row2[i]) == 0 for i in range(len(row1))]
    sim = diff.count(True)
    perc = sim / len(row1)
    dset[id1, id2] = perc

f.close()

""" How to read it
f = h5py.File("output.hdf5", "r")
dset = f.get("perc_sim")
print(dset[1,1])
f.close()
"""
